source 'https://rubygems.org'

gem 'rails'
gem 'mysql2', '>= 0.4.4', '< 0.6.0'
gem 'puma', '~> 3.11'
gem "geocoder"
gem 'delayed_job_active_record'

# Assets and JavaScript
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
gem 'high_voltage'
gem 'jquery-rails'
gem "font-awesome-rails"
gem 'ajax-datatables-rails'

# App Gems / Auth
gem 'devise'
gem "gretel"
gem "pundit"

# Deployment with Capistrano
gem 'capistrano'
gem 'capistrano-rvm'
gem 'capistrano-bundler'
gem 'capistrano-rails'
gem 'capistrano-passenger'
gem 'capistrano-maintenance', '~> 1.0', require: false
gem "airbrussh", :require => false
gem 'capistrano3-delayed-job'
gem 'capistrano-rails-console'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'rspec-rails', '~> 3.5'
  gem 'factory_girl_rails'
  gem 'faker'
  gem 'capybara'
  gem 'guard-rspec'
  gem 'spring-commands-rspec'
  gem 'rails-controller-testing'
  gem 'database_cleaner'
  gem 'launchy'
  gem 'selenium-webdriver'
  gem 'simplecov', :require => false
  gem 'rubocop', '~> 0.52.1', require: false
  gem 'pry'
  gem 'pry-rails'
  gem 'pry-doc'
  gem 'rb-readline'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console'
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
