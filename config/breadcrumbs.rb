crumb :root do
  link "Home", root_path
end

crumb :job_posts do
  link "jobs", job_posts_path
end

crumb :new_job_post do |job_post|
  link "New"
  parent :job_posts
end

crumb :job_listing do
  link "Listing"
  parent :job_posts
end