# config valid only for current version of Capistrano
# lock '3.4.0'

set :application, 'goget'
set :repo_url, 'git@bitbucket.org:hafizz08/goget.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'
set :deploy_to, '/home/ubuntu/apps/goget'

# Default value for :scm is :git
# set :scm, :git
set :scm, :git
set :pty, true
# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/master.key','config/credentials.yml.enc')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/static', 'public/media','storage')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Resque configurations
# set :resque_environment_task, true
# set :resque_log_file, "log/resque.log"
# set :resque_environment_task, true

namespace :deploy do

  desc "restart apache server"
  task :restart do
    on roles(:app) do
      sudo "/etc/init.d/apache2 restart"
    end
  end

  after 'deploy:assets:backup_manifest', 'maintenance:enable'
  after 'deploy:publishing', 'deploy:restart'
  after 'deploy:restart', 'maintenance:disable'
  # after 'maintenance:disable', "resque:restart"
  # after 'resque:restart', "resque:scheduler:restart"

end

namespace :logs do
  desc "tail rails logs"
  task :tail do
    on roles(:app) do
      execute "tail -f #{shared_path}/log/staging.log"
    end
  end
end