Rails.application.routes.draw do
  devise_for :users, controllers: {
  	registrations: 'users/registrations',
  	sessions: "users/sessions",
  	passwords: "users/passwords",
  }

  devise_scope :user do
    authenticated :user do
      root 'job_posts#new', as: :root
    end

    unauthenticated do
      root 'users/sessions#new', as: :unauthenticated_root
    end
  end

  resources :job_posts, path: 'jobs' do
    put :claim
  	match '/listing' =>  'job_posts#listing', as: :jobs_listing, via: [:get], on: :collection
  end
end