class ChangeColumnJobPostStatus < ActiveRecord::Migration[5.2]
  def change
  	change_column :job_posts, :status, :integer, default: 0
  end
end