class CreateJobPosts < ActiveRecord::Migration[5.2]
  def change
    create_table :job_posts do |t|
    	t.text :pickup_address
    	t.decimal :pickup_latitude, precision: 12, scale: 9
    	t.decimal :pickup_longitude, precision: 12, scale: 9
    	t.text :dropoff_address
    	t.decimal :dropoff_latitude, precision: 12, scale: 9
    	t.decimal :dropoff_longitude, precision: 12, scale: 9
    	t.integer :hirer_id
    	t.integer :hiree_id
    	t.integer :status
    	t.timestamps null: false
    end
    add_index :job_posts, :hirer_id
    add_index :job_posts, :hiree_id
  end
end