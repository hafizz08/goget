// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require rails-ujs
//= require popper.js/dist/umd/popper.min.js
//= require moment/moment.js
//= require chart.js/dist/Chart.min.js
//= require select2/dist/js/select2.full.min.js
//= require jquery-bar-rating/dist/jquery.barrating.min.js
//= require bootstrap-validator/dist/validator.min.js
//= require bootstrap-daterangepicker/daterangepicker.js
//= require ion.rangeSlider/js/ion.rangeSlider.min.js
//= require dropzone/dist/dropzone.js
//= require editable-table/mindmup-editabletable.js
//= require datatables.net/js/jquery.dataTables.min.js
//= require datatables.net-bs/js/dataTables.bootstrap.min.js
//= require fullcalendar/dist/fullcalendar.min.js
//= require perfect-scrollbar/js/perfect-scrollbar.jquery.min.js
//= require tether/dist/js/tether.min.js
//= require slick-carousel/slick/slick.min.js
//= require bootstrap/js/dist/util.js
//= require bootstrap/js/dist/alert.js
//= require bootstrap/js/dist/button.js
//= require bootstrap/js/dist/carousel.js
//= require bootstrap/js/dist/collapse.js
//= require bootstrap/js/dist/dropdown.js
//= require bootstrap/js/dist/modal.js
//= require bootstrap/js/dist/tab.js
//= require bootstrap/js/dist/tooltip.js
//= require bootstrap/js/dist/popover.js
//= require bootstrap/js/dist/bootstrap-waitingfor.min.js
//= require main.js
//= require_tree .