if( $('.google_map_section').length ) {
    var geocoder;
    var map;
    var marker;
    var dropoff_geocoder;
    var dropoff_map;
    var dropoff_marker;
    /*
     * Google Map with marker
     */
    function initializeGmap() {
        // Start setup default options for Map
        var initialLat = $('.pickup_latitude').val();
        var initialLong = $('.pickup_longitude').val();
        initialLat = initialLat?initialLat:3.1432422;
        initialLong = initialLong?initialLong:101.70923130000006;

        var latlng = new google.maps.LatLng(initialLat, initialLong);
        var options = {
            zoom: 16,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById("geomap"), options);

        geocoder = new google.maps.Geocoder();

        marker = new google.maps.Marker({
            map: map,
            draggable: true,
            position: latlng
        });
        // End setup default options for Map

        // Start Drag Icon Pin Event
        google.maps.event.addListener(marker, "dragend", function () {
            var point = marker.getPosition();
            map.panTo(point);
            geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    marker.setPosition(results[0].geometry.location);
                    $('.pickup_addr').val(results[0].formatted_address);
                    $('.pickup_latitude').val(marker.getPosition().lat());
                    $('.pickup_longitude').val(marker.getPosition().lng());
                }
            });
        });
        // End Drag Icon Pin Event

        // Start Autocomplete Addess Search
        var input = document.getElementById('search_location');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        autocomplete.addListener('place_changed', function() {
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                  (place.address_components[0] && place.address_components[0].short_name || ''),
                  (place.address_components[1] && place.address_components[1].short_name || ''),
                  (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            $('.pickup_latitude').val(place.geometry.location.lat());
            $('.pickup_longitude').val(place.geometry.location.lng());
        });
        // End Autocomplete Addess Search

        // Start search by latitude and longitude
        document.getElementById('submit-latlng').addEventListener('click', function() {
          var inputLat = $('.pickup_latitude').val();
          var inputLong = $('.pickup_longitude').val();
          var latlng = {lat: parseFloat(inputLat), lng: parseFloat(inputLong)};
          geocoder.geocode({'location': latlng}, function (results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                  map.setCenter(results[0].geometry.location);
                  marker.setPosition(results[0].geometry.location);
                  $('.pickup_addr').val(results[0].formatted_address);
                  $('.pickup_latitude').val(marker.getPosition().lat());
                  $('.pickup_longitude').val(marker.getPosition().lng());
              } else {
                alert('No results found');
              }
          });
        });
        // End search by latitude and longitude

    }

    function initializeDropOffGmap() {
        // Start setup default options for Map
        var dropinitialLat = $('.dropoff_latitude').val();
        var dropinitialLong = $('.dropoff_longitude').val();
        dropinitialLat = dropinitialLat?dropinitialLat:3.1432422;
        dropinitialLong = dropinitialLong?dropinitialLong:101.70923130000006;

        var droplatlng = new google.maps.LatLng(dropinitialLat, dropinitialLong);
        var dropoff_options = {
            zoom: 16,
            center: droplatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        dropoff_map = new google.maps.Map(document.getElementById("dropoff-geomap"), dropoff_options);

        dropoff_geocoder = new google.maps.Geocoder();

        dropoff_marker = new google.maps.Marker({
            map: dropoff_map,
            draggable: true,
            position: droplatlng
        });
        // End setup default options for Map

        // Start Drag Icon Pin Event
        google.maps.event.addListener(dropoff_marker, "dragend", function () {
            var dropoff_point = dropoff_marker.getPosition();
            dropoff_map.panTo(dropoff_point);
            geocoder.geocode({'latLng': dropoff_marker.getPosition()}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    dropoff_marker.setPosition(results[0].geometry.location);
                    $('.dropoff_addr').val(results[0].formatted_address);
                    $('.dropoff_latitude').val(dropoff_marker.getPosition().lat());
                    $('.dropoff_longitude').val(dropoff_marker.getPosition().lng());
                }
            });
        });
        // End Drag Icon Pin Event

        // Start Autocomplete Addess Search
        var dropoff_input = document.getElementById('search_dropoff_location');
        var dropoff_autocomplete = new google.maps.places.Autocomplete(dropoff_input);
        dropoff_autocomplete.bindTo('bounds', dropoff_map);

        dropoff_autocomplete.addListener('place_changed', function() {
            dropoff_marker.setVisible(false);
            var dropoff_place = dropoff_autocomplete.getPlace();
            if (!dropoff_place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (dropoff_place.geometry.viewport) {
                dropoff_map.fitBounds(dropoff_place.geometry.viewport);
            } else {
                dropoff_map.setCenter(dropoff_place.geometry.location);
                dropoff_map.setZoom(17);
            }
            dropoff_marker.setPosition(dropoff_place.geometry.location);
            dropoff_marker.setVisible(true);

            var dropoff_address = '';
            if (dropoff_place.address_components) {
                dropoff_address = [
                  (dropoff_place.address_components[0] && dropoff_place.address_components[0].short_name || ''),
                  (dropoff_place.address_components[1] && dropoff_place.address_components[1].short_name || ''),
                  (dropoff_place.address_components[2] && dropoff_place.address_components[2].short_name || '')
                ].join(' ');
            }

            $('.dropoff_latitude').val(dropoff_place.geometry.location.lat());
            $('.dropoff_longitude').val(dropoff_place.geometry.location.lng());
        });
        // End Autocomplete Addess Search

        // Start search by latitude and longitude
        document.getElementById('submit-dropoff-latlng').addEventListener('click', function() {
          var dropinputLat = $('.dropoff_latitude').val();
          var dropinputLong = $('.dropoff_longitude').val();
          var droplatlng = {lat: parseFloat(dropinputLat), lng: parseFloat(dropinputLong)};
          geocoder.geocode({'location': droplatlng}, function (results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                  dropoff_map.setCenter(results[0].geometry.location);
                  dropoff_marker.setPosition(results[0].geometry.location);
                  $('.dropoff_addr').val(results[0].formatted_address);
                  $('.dropoff_latitude').val(dropoff_marker.getPosition().lat());
                  $('.dropoff_longitude').val(dropoff_marker.getPosition().lng());
              } else {
                alert('No results found');
              }
          });
        });
        // End search by latitude and longitude

    }
    // Load google map
    initializeGmap();
    initializeDropOffGmap();
    /*
     * Point location on google map
     */
    $('.get_map').click(function (e) {
        var address = $(PostCodeid).val();
        geocoder.geocode({'address': address}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                marker.setPosition(results[0].geometry.location);
                $('.pickup_addr').val(results[0].formatted_address);
                $('.pickup_latitude').val(marker.getPosition().lat());
                $('.pickup_longitude').val(marker.getPosition().lng());
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
        e.preventDefault();
    });

    //Add listener to marker for reverse geocoding
    google.maps.event.addListener(marker, 'drag', function () {
        geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                	$('.pickup_addr').val(results[0].formatted_address);
                    $('.pickup_latitude').val(marker.getPosition().lat());
                    $('.pickup_longitude').val(marker.getPosition().lng());
                }
            }
        });
    });
}