class ApplicationController < ActionController::Base
  include Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  before_action lambda { @body_class = 'menu-position-side menu-side-left full-screen with-content-panel' }
  before_action lambda { @div_class = 'with-side-panel solid-bg-all' }
  before_action :authenticate_user!
  protect_from_forgery prepend: true, with: :null_session
  layout 'dashboard'

  def after_sign_in_path_for(resource)
    root_path
  end

  private
  def user_not_authorized
    flash[:alert] = "You are not authorized to perform this action."
    redirect_to(request.referrer || root_path)
  end
end