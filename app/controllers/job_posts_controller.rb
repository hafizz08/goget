class JobPostsController < ApplicationController
  before_action :set_job, only: [:show, :edit, :update, :destroy]
  before_action :set_job_claim, only: [:claim]
  before_action :current_location, only: [:listing]

  def index
    authorize JobPost
    @hirer_jobs = JobPost.as_hirer(current_user)
    @hiree_jobs = JobPost.as_hiree(current_user)

    respond_to do |format|
      format.html
      format.json { head :no_content }
    end
  end

  def listing
    authorize JobPost
    @jobs = JobPost.available.near(current_location, 1000, :units => :km, :order => 'distance')

    respond_to do |format|
      format.html
      format.json { head :no_content }
    end
  end

  def claim
    authorize @job
    @job.update_attributes(status: :claimed, hiree: current_user)
    respond_to do |format|
      format.html { redirect_to job_posts_path, notice: 'Job successfully claimed.' }
      format.json { head :no_content }
    end
  end

  def new
    authorize JobPost
    @job = JobPost.new
    respond_to do |format|
      format.html
      format.json { head :no_content }
    end
  end

  def edit
    authorize @job
    respond_to do |format|
        format.html
        format.json { head :no_content }
    end
  end

  def create
    authorize JobPost
    @job = JobPost.new(job_post_params.merge hirer: current_user)

    respond_to do |format|
      if @job.save!
        format.html { redirect_to job_posts_path, notice: 'Job was successfully created.' }
        format.json { render action: 'show', status: :created, location: @job }
      else
        format.html { render action: 'new' }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    authorize @job
    respond_to do |format|
      if @job.update(job_post_params)
        format.html { redirect_to job_posts_path, notice: 'Job was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    authorize @job
    @job.destroy
    respond_to do |format|
      format.html { redirect_to job_posts_path }
      format.json { head :no_content }
    end
  end

  private
    def set_job
      @job = JobPost.find(params[:id])
    end

    def set_job_claim
      @job = JobPost.find(params[:job_post_id])
    end

    def current_location
        user_ip_address = if Rails.env.staging?
          current_user.current_sign_in_ip
        else
          Net::HTTP.get(URI.parse('http://checkip.amazonaws.com/')).squish
        end
        location = Geocoder.search(user_ip_address).first
        [location.latitude, location.longitude]
    end

    def job_post_params
      params.require(:job_post).permit(:title, :pickup_address, :pickup_latitude, :pickup_longitude, :dropoff_address,
                                       :dropoff_latitude, :dropoff_longitude, :hirer_id, :hiree_id)
    end
end