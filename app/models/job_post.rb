class JobPost < ActiveRecord::Base
  belongs_to :hirer, class_name: "User", foreign_key: 'hirer_id', optional: true
  belongs_to :hiree, class_name: "User", foreign_key: 'hiree_id', optional: true

  scope :as_hirer, ->(id) {where(hirer: id)}
  scope :as_hiree, ->(id) {where(hiree: id)}
  scope :available, -> {where(hiree: nil)}

  enum status: [:unclaimed, :claimed]

  geocoded_by :pickup_address, latitude: :pickup_latitude, longitude: :pickup_longitude

  def self.distance
    self.distance_to([self.pickup_latitude,self.pickup_longitude], :km).round(2) * 1000
  end
end