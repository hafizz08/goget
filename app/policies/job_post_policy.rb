class JobPostPolicy < ApplicationPolicy

  def index?
    true
  end

  def listing?
    true
  end

  def claim?
    record.hirer != user && record.hiree.nil?
  end

  def show?
    true
  end

  def update?
    record.hirer == user
  end

  def new?
    true
  end

  def create?
    true
  end

  def edit?
    record.hirer == user
  end

  def destroy?
    record.hirer == user
  end

  class Scope
    attr_reader :user, :scope

    def initialize(current_admin, scope)
      @current_admin = current_admin
      @scope = scope
    end
  end

end