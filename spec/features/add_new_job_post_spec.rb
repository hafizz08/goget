require 'rails_helper'

RSpec.feature "AddNewJobPosts", type: :feature do
  it "should require the user log in before adding a post" do
    password = "123456789"
    u = create( :user, name: "person", password: password, password_confirmation: password )

    visit new_job_post_path

    within "#new_user" do
      fill_in "user_email", with: u.email
      fill_in "user_password", with: password
    end

    click_button "Log me in"

    within "#new_job_post" do
      fill_in "job_post[pickup_address]", with: "Berjaya Times Square, Imbi, Kuala Lumpur, Federal Territory of Kuala Lumpur, Malaysia"
      fill_in "job_post[pickup_latitude]", with: "3.1421984"
      fill_in "job_post[pickup_longitude]", with: "101.7105512"
      fill_in "job_post[dropoff_address]", with: "Nexus Bangsar South, Jalan Kerinchi, Bangsar South, Kuala Lumpur, Federal Territory of Kuala Lumpur, Malaysia"
      fill_in "job_post[dropoff_latitude]", with: "3.10968"
      fill_in "job_post[dropoff_longitude]", with: "101.6653807"
    end

    click_link_or_button "Create"

    expect( JobPost.count ).to eq(1)
    expect( JobPost.first.pickup_address).to eq("Berjaya Times Square, Imbi, Kuala Lumpur, Federal Territory of Kuala Lumpur, Malaysia")
  end
end
